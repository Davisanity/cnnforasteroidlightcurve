import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
model_path = "/model/saved_model.ckpt"
import random
import time
# for debugging, outputs are Opetation name & Array size which you're using now.
def print_activations(t):
    print(t.op.name, " ", t.get_shape().as_list())

# During Training, CNN model's input would be batch Image(that is, x), at that moment, generate OneHotLabel(that is,y)
# Args: data is a full path. ex: data\shapeV\32033.png 
def getOneHot(data):
    catogories=3
    y=np.zeros([len(data),catogories])
    for i in range(len(data)):
        data[i] = str(data[i],'utf-8')
        if data[i].find('shapeW')!=-1 or data[i].find('genW')!=-1:
            y[i][0]=1
        elif data[i].find('shapeV')!=-1 or data[i].find('genV')!=-1:
            y[i][1]=1
        else:
            y[i][2]=1
    return y

# Args: train for training data, test for testing data
# save data into an array
# Return: now is [pathlist] ex: ['test//gen/v_0_8987.png'...]
def getData(arg):
    path_list=[]
    for root,dirs,files in os.walk(arg+'//'):
        # print(root) #data\ or data\other
        for f in files:
            p = os.path.join(root,f)
            path_list.append(p)
            # print(os.path.join(root,f)) #data\other\32033.png
    return path_list

# Args: img_path is a path list of image. ex: ['data\other\32033.png','data\other\32533.png'...]
# Original image is 400x300, I'll resize it into 300x300
# Return: an nparray(300,300)
# About PIL library: https://blog.csdn.net/icamera0/article/details/50647465
# About PIL library: https://blog.csdn.net/majinlei121/article/details/78933947
def img_to_array(img_path):
    l = len(img_path) #l is 5, that is batch_size
    img_list=np.zeros((l,300,300,1))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).reshape(300,300,1)
        img_list[i]=img
        img_list[i]=img_list[i]/255
    # print("img_list.shape: ",img_list.shape) # output:(len(training_data_path),300,300,1))
    #img_list is a tuple so not img_list.shape()[tuple can't call function, img_list.shape is an attribute]
    return img_list

# Args: data is image array([L,300,300,1]),L is image numbers; label is label array([L,3]);
# batch_size is size per batch;iter_num is how mant epochs you want, when reach iter_num, it would get OutOfRange Error
# Return: a batch. When calling batch.next() you can get batch.
# def get_Batch(data, label, batch_size, iter_num):
def get_Batch(path, batch_size, iter_num):
    input_queue = tf.train.slice_input_producer([path], num_epochs=iter_num, shuffle=True, capacity=32 )
    p_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
    print(p_batch)
    return p_batch

def model_add(s_name,k_name,k_shape,input,b_name,b_shape):
    with tf.name_scope(s_name) as scope:
        kernel = tf.Variable(tf.truncated_normal(k_shape, dtype=tf.float32, stddev=1, name=k_name))
        tf.summary.histogram(scope+'/'+k_name, kernel)
        conv = tf.nn.conv2d(input, kernel, [1, 1, 1, 1], padding="SAME")
        bias = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=b_shape), trainable=True, name=b_name)
        tf.summary.histogram(scope+'/'+b_name, bias)
        bias = tf.nn.bias_add(conv, bias)
        conv = tf.nn.relu(bias, name=scope)
    return kernel,bias,conv    

def dense_add(k_name,k_shape,input,b_name,b_shape,keepprob):
    with tf.name_scope("dense") as scope:
        kernel = tf.Variable(tf.truncated_normal(k_shape, dtype=tf.float32, stddev=0.1, name=k_name))
        bias = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=b_shape), trainable=True, name=b_name)
        flatten = tf.reshape(input,[-1,k_shape[0]])
        fc = tf.nn.relu(tf.matmul(flatten,kernel)+bias)
        dropout=tf.nn.dropout(fc,keepprob)
    return kernel,bias,dropout

def output_add(k_name,k_shape,input,b_name,b_shape):
    with tf.name_scope("output") as scope:
        kernel = tf.Variable(tf.truncated_normal(k_shape, dtype=tf.float32, stddev=0.1, name=k_name))
        tf.summary.histogram("output/"+k_name, kernel)
        bias = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=b_shape), trainable=True, name=b_name)
        tf.summary.histogram("output/"+b_name, bias)
        output = tf.matmul(input,kernel) + bias
    return kernel,bias,output

#CNN
#After Pooling : (W-F+1)/S , W for input, F for pool size, S for stride
def CNN(image, keepprob):
    kernel1,bias1,conv1 = model_add("conv1","weight1",[5,5,1,32],image,"bias1",[32])
    pool1 = tf.nn.max_pool(conv1,ksize=[1,5,5,1],strides=[1,2,2,1],padding="VALID",name="pool1")
    # print(pool1)#?, 148, 148, 32
    kernel2,bias2,conv2 = model_add("conv2","weight2",[5,5,32,64],pool1,"bias2",[64])
    pool2 = tf.nn.max_pool(conv2,ksize=[1,5,5,1],strides=[1,2,2,1],padding="VALID",name="pool2")
    # print(pool2) #?, 72, 72, 64
    kernel3,bias3,conv3 = model_add("conv3","weight3",[5,5,64,128],pool2,"bias3",[128])
    pool3 = tf.nn.max_pool(conv3,ksize=[1,5,5,1],strides=[1,2,2,1],padding="VALID",name="pool3")
    # print(pool3) #?, 34, 34, 128
    kernel4,bias4,conv4 = model_add("conv4","weight4",[5,5,128,256],pool3,"bias4",[256])
    pool4 = tf.nn.max_pool(conv4,ksize=[1,5,5,1],strides=[1,2,2,1],padding="VALID",name="pool4")
    # print(pool3) #?, 15, 15, 256
    kernel5,bias5,dropout = dense_add("weight5",[15*15*256,512],pool4,"bias5",[512],keepprob=keepprob)
    kernel6,bias6,output = output_add("weight6",[512,3],dropout,"bias6",[3])
    return output

# def CNN(image, keepprob=0.7):
#     with tf.name_scope("conv1") as scope:
#         kernel1 = tf.Variable(tf.truncated_normal([5, 5, 1, 32], dtype=tf.float32, stddev=1, name="weight1"))
#         tf.summary.histogram('conv1/kernel1', kernel1)
#         conv1 = tf.nn.conv2d(image, kernel1, [1, 1, 1, 1], padding="SAME")
#         biases1 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[32]), trainable=True, name="bias1")
#         tf.summary.histogram('conv1/bias1', biases1)
#         bias1 = tf.nn.bias_add(conv1, biases1)
#         conv1 = tf.nn.relu(bias1, name=scope)
#     pool1 = tf.nn.max_pool(conv1,ksize=[1,10,10,1],strides=[1,3,3,1],padding="VALID",name="pool1")
#     # print(pool1)#?, 97, 97, 32

#     with tf.name_scope("conv2") as scope:
#         kernel2 = tf.Variable(tf.truncated_normal([5, 5, 32, 64], dtype=tf.float32, stddev=0.1, name="weight2"))
#         tf.summary.histogram('conv2/kernel2', kernel2)
#         conv2 = tf.nn.conv2d(pool1, kernel2, [1, 1, 1, 1], padding="SAME")
#         biases2 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[64]), trainable=True, name="bias2")
#         tf.summary.histogram('conv1/bias2', biases2)
#         bias2 = tf.nn.bias_add(conv2, biases2)
#         conv2 = tf.nn.relu(bias2, name=scope)
#     pool2 = tf.nn.max_pool(conv2,ksize=[1,10,10,1],strides=[1,3,3,1],padding="VALID",name="pool2")
#     # print(pool2) #?, 30, 30, 64

#     with tf.name_scope("conv3") as scope:
#         kernel3 = tf.Variable(tf.truncated_normal([5, 5, 64, 128], dtype=tf.float32, stddev=0.1, name="weight3"))
#         tf.summary.histogram('conv3/kernel3', kernel3)
#         conv3 = tf.nn.conv2d(pool2, kernel3, [1, 1, 1, 1], padding="SAME")
#         biases3 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[128]), trainable=True, name="bias3")
#         tf.summary.histogram('conv3/bias3', biases3)
#         bias3 = tf.nn.bias_add(conv3, biases3)
#         conv3 = tf.nn.relu(bias3, name=scope)
#     pool3 = tf.nn.max_pool(conv3,ksize=[1,3,3,1],strides=[1,2,2,1],padding="VALID",name="pool2")
#     # print(pool3) #?, 14, 14, 128
#     # (W-F+1) /S = (30-3+1)/2

#     with tf.name_scope("dense") as scope:
#         weight4 = tf.Variable(tf.truncated_normal([14*14*128,1024], dtype=tf.float32, stddev=0.1, name="weight4"))
#         bias4 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[1024]), trainable=True, name="bias4")
#         flatten1 = tf.reshape(pool3,[-1,14*14*128])
#         fc1 = tf.nn.relu(tf.matmul(flatten1,weight4)+bias4)
#         dropout1=tf.nn.dropout(fc1,keepprob)

#     with tf.name_scope("output") as scope:
#         weight5 = tf.Variable(tf.truncated_normal([1024,3], dtype=tf.float32, stddev=0.1, name="weight5"))
#         bias5 = tf.Variable(tf.constant(0.1, dtype=tf.float32, shape=[3]), trainable=True, name="bias5")
#         output = tf.matmul(dropout1,weight5) + bias5
#         print(output)
#     return output


def main():
    # Parameters
    # iter num = ( data_size/batch_size ) * iter_num
    iter_num=10   #   21440 /  300  = 71 * 6 = 426  
    batch_size=5
    tbatch_size=5
    lrn_rate=1e-3
    checkpoint=10
    # with tf.device('/cpu:0')
    # Get datas and data pre-processing
    train_data_path = getData('train')
    train_data_size = len(train_data_path) #18000
    test_data_path = getData('train')
    test_data_size = len(test_data_path) #700

    # with tf.device('/gpu:0'):  
    tfinput = tf.placeholder(dtype=tf.float32, shape=[None, 300, 300,1], name='input')
    tfoutput = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='output')
    tflabel = tf.placeholder(dtype=tf.float32, shape=[None, 3], name='label')
    tfoutput = CNN(tfinput, 0.7)

    # x_batch,y_batch = get_Batch(train_img,train_label,batch_size,iter_num)
    # tx_batch,ty_batch=get_Batch(test_img,test_label,tbatch_size,iter_num)
    p_batch = get_Batch(train_data_path,batch_size,iter_num)
    tp_batch=get_Batch(test_data_path,tbatch_size,iter_num)
    with tf.name_scope('Loss'):
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=tfoutput, labels=tflabel))
        tf.summary.scalar('Loss', loss)
    with tf.name_scope('Train'):
        # optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.09).minimize(loss)
        trainstep = tf.train.AdamOptimizer(learning_rate=lrn_rate).minimize(loss)
    with tf.name_scope('Accuracy'):
        accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(tfoutput, 1),tf.argmax(tflabel, 1)),tf.float32))
        tf.summary.scalar('Accuracy', accuracy)
    saver = tf.train.Saver()
    config = tf.ConfigProto(log_device_placement=False)
    merged = tf.summary.merge_all()
    # config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        writer = tf.summary.FileWriter("TensorBoard/", graph = sess.graph) #For TensorBoard
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess, coord)
        epoch=1
        start = time.time()
        try:
            while not coord.should_stop():                
                # batch_input,batch_label=sess.run([x_batch,y_batch])
                batch_path = sess.run(p_batch)
                batch_input = img_to_array(batch_path)
                batch_label = getOneHot(batch_path)
                a, b,c = sess.run([trainstep, loss,tfoutput], feed_dict={tfinput: batch_input, tflabel: batch_label})
                print("Begin epoch: ",epoch," ,loss: ",b," output:",c)
                # saver.save(sess, "model/model.ckpt",global_step=15,write_meta_graph=False ) 
                epoch+=1
                if epoch%checkpoint==0:
                    batch_t_path = sess.run(tp_batch)
                    batch_t_input = img_to_array(batch_t_path)
                    batch_t_label = getOneHot(batch_t_path)
                    acc = accuracy.eval(feed_dict = {tfinput: batch_t_input, tflabel: batch_t_label})
                    print("step %d, training accuracy %g"%(epoch, acc))
                    summary = sess.run(merged, feed_dict={tfinput: batch_input, tflabel: batch_label})
                    writer.add_summary(summary,epoch)

        except tf.errors.OutOfRangeError:
            # accuracy = tf.reduce_mean(tf.cast(tf.equal(tf.argmax(out, 1),tf.argmax(train_label, 1)),tf.float32))
            # tf.summary.scalar("Accuracy", accuracy)

            end = time.time()
            elapsed = end - start
            saver.save(sess, "model/model.ckpt")
            # can add this parameter:global_step=100 , write_meta_graph=False
            # save model per global_step
            
            print("---end---Time: ", elapsed, "seconds.")
        finally:
            coord.request_stop()
            print("--p end---")
        coord.join(threads)
        


if __name__ == '__main__':
    main()
    # a = getData('test')
    # print(a[0])

