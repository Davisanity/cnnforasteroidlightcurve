import numpy as np
from keras import models
from keras.models import Sequential,load_model,Model
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras import backend as K
import matplotlib.pyplot as plt
from PIL import Image
import PIL.ImageOps 

def img_to_array(img_path):
    l = len(img_path) #l is 5, that is batch_size
    img_list=np.zeros((l,300,300,1))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).reshape(300,300,1)
        img_list[i]=img
        img_list[i]=img_list[i]/255
    return img_list

def main():
	model = load_model('keras_model\\4cnn.h5') #replaced by your model name
	img_path=['Kernel\\w1.png','Kernel\\w2.png','Kernel\\v1.png','Kernel\\v2.png','Kernel\\o1.png','Kernel\\o2.png','Kernel\\a1.png','Kernel\\a2.png','Kernel\\a3.png']
	# img_path=['Kernel\\castle.png']
	img_list=img_to_array(img_path)
	input_img = np.expand_dims(img_list[4],axis=0)
	# layer = 'conv2d_1'
	# shape = [296,142]
	num_layer = 8
	# outputs = 32
	rows = 8
	# columns = outputs/rows
	# conv_model = Model(input=model.input,output=model.get_layer(layer).output)
	# output=conv_model.predict(img_list)
	# print(output.shape) #(296, 296, 32)
	# for i in range(outputs):
	# 	plt.subplot(rows,columns,i+1)
	# 	plt.matshow(output[0,:,:,outputs-1],cmap='gray_r',fignum=False)
	# 	plt.xticks([])
	# 	plt.yticks([])

	layer_outputs = [layer.output for layer in model.layers[:num_layer]] # Extracts the outputs of the top 12 layers
	activation_model = models.Model(inputs=model.input, outputs=layer_outputs) # Creates a model that will return these outputs, given the model input
	activations = activation_model.predict(input_img) 

	layer_names = []
	for layer in model.layers[:num_layer]:
		layer_names.append(layer.name) # Names of the layers, so you can have them as part of your plot   

	for layer_name, layer_activation in zip(layer_names, activations): # Displays the feature maps
		n_features = layer_activation.shape[-1] # Number of features in the feature map
		size = layer_activation.shape[1] #The feature map has shape (1, size, size, n_features).
		n_cols = n_features // rows # Tiles the activation channels in this matrix
		display_grid = np.zeros((size * n_cols, rows * size))
		for col in range(n_cols): # Tiles each filter into a big horizontal grid
			for row in range(rows):
				channel_image = layer_activation[0,:, :,col * rows + row]
				channel_image -= channel_image.mean() # Post-processes the feature to make it visually palatable
				channel_image /= channel_image.std()
				channel_image *= 64
				channel_image += 128
				channel_image = np.clip(channel_image, 0, 255).astype('uint8')
				display_grid[col * size : (col + 1) * size, # Displays the grid
	                         row * size : (row + 1) * size] = channel_image
		scale = 1. / size
		plt.figure(figsize=(scale * display_grid.shape[1],
	                        scale * display_grid.shape[0]))
		plt.title(layer_name)
		plt.grid(False)
		plt.imshow(display_grid, aspect='auto', cmap='Greys')


	#####Tried To Squeeze all kernel to one image->Wrong way
	# o = output[0]
	# o = np.expand_dims(o,axis=0)
	# print(o.shape)
	# o = np.squeeze(o,axis=0)
	# print(o.shape)
	# plt.imshow(o)
	# plt.show()
	# print(output.shape) #(296, 296, 32)
	# plt.figure()
	####FOR All Datas######
	# for i in range(9):
	# 	plt.subplot(3,3,i+1)
	# 	plt.matshow(output[i,:,:,outputs-1],cmap='gray_r',fignum=False)
	# 	plt.xticks([])
	# 	plt.yticks([])# ####For All Kernels####
	# plt.tight_layout(pad=0, w_pad=0, h_pad=0)
	plt.show()

if __name__ == '__main__':
    main()