# CNNforAsteroidLightCurve

This is the code of my graduation thesis , which topic is "Classification for the Rotation Periods of Asteroids Using the Convolution Neural Network"
The code uses Tensorflow to implement the algorithm, also uses GPU to do Parallel operation.

If you want to execute keras_train.py and keras_predict.py, you need to pass args to execute:

參數說明如下:
keras_train.py
名稱 batch_size  epochs  learing_rate  using_model    log_dir             model_name
型態     int       int        float        string      string                string
範例     100        10         1e-3        '6cnn'   './keras_log/name'   './keras_model/name.h5'

說明:
batch_size   是指一次訓練時丟入多少圖片訓練,通常到150就已經是很大的數目了
epochs       是指所有資料總共訓練幾次，因為本實驗資料量夠，所以2次就已經收斂了
lrn_rate     是指學習率，通常都設定為0.001
using_model  是指使用哪一個網路模型，程式裡已經寫好 2cnn 4cnn 6cnn 8cnn vgg16 及 ResNet(需要夠大的記憶體才跑得動)
             你下 2cnn 就會呼叫程式裡的 Cnn2()函式，裡面就是2cnn的網路模型，依此類推
log_dir      是指將Tensorboard視覺化網路架構圖、訓練歷程、準確率及loss的資訊存在哪裡
model_name   是指訓練完成後的模型要存在哪裡，在預測時直接載入訓練好的模型即可

keras_predict.py
名稱 predict_data       model_name       output_layer_name  threshold
型態   string            string              string           float
範例   'data'    'keras_model/6cnn.h5'      'dense_3'         0.91

說明:
predict_data 是指需要進行分類的檔案"目錄"
model_name   是指要載入來分類的模型名稱
o_layer_name 是指該模型的softmax層的名字，因為該層的輸出就是預測為該分類的機率
             本程式中除了vgg16模型的softmax是 'dense_4' 以外 其他都是 'dense_3'
threshold    是閥值，機率大於該閥值的才會被分類，否則都當作ambigious

整體環境的目錄結構如下:
  
  
            /keras_train.py
            /keras_predict.py
    /project-keras_model -- 6cnn.h5 8cnn.h5 ... vgg16.h5
             \keras_log   -- 6cnn 8cnn ... vgg16
            \       /shapeW
             \      /genW
              \train-shapeV   (train valid都是一樣的結構 不過valid沒有genW genV)
               valid-genV
                    \other
                      /W
              \predict-V
                      \O
                      \A  
                  

predict及子目錄的結構要事先創建好