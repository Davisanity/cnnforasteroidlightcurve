import tensorflow as tf
import os
import matplotlib.pyplot as plt
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
import random

# def yield_fun():
#     a = 3
#     b = 2
#     yield a
#     c = 4
#     yield b
     
# generator = yield_fun()
# print(next(generator))
# print(next(generator))

# path = ['a','b','c','d','e','f','g','h','i','j','k','l','m']
def getData(arg):
    path_list=[]
    for root,dirs,files in os.walk(arg+'//'):
        # print(root) #data\ or data\other
        for f in files:
            p = os.path.join(root,f)
            path_list.append(p)
            # print(os.path.join(root,f)) #data\other\32033.png
    # labels=getOneHot(path_list)
    # datas=zip(path_list,labels)
    # # print(list(datas))
    # return list(datas)
    return path_list

def get_Batch(path, batch_size, iter_num):
    # input_queue = tf.train.slice_input_producer([data, label], num_epochs=iter_num, shuffle=True, capacity=32 ) 
    # x_batch, y_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
    # return x_batch, y_batch
    input_queue = tf.train.slice_input_producer([path], num_epochs=iter_num, shuffle=True, capacity=32 )
    p_batch = tf.train.batch(input_queue, batch_size=batch_size, num_threads=1, capacity=32, allow_smaller_final_batch=True)
    return p_batch

def main():
    path = getData('test')
    p_batch = get_Batch(path,3,10)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())
        coord = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess, coord)
        epoch=1
        try:
            while not coord.should_stop():                
                    batch_path = sess.run(p_batch)
                    # for i in batch_path:
                    #     i = str(i,'utf-8')
                    #     print(i)
                    batch_path[0] = str(batch_path[0],'utf-8')
                    if batch_path[0].find('shapeW')!=-1:
                        print("WWW")
                    elif batch_path[0].find('shapeV')!=-1:
                        print("VVV")
                    else:
                        print("OOOO")
                    print("epoch: "+str(epoch),"path: "+str(batch_path))
                    # print(type(batch_path))
                    # print(len(batch_path))
                    epoch+=1
        except tf.errors.OutOfRangeError:
                print("---end---")
        finally:
                coord.request_stop()
                print("--p end---")
        coord.join(threads)
        
if __name__ == '__main__':
    main()