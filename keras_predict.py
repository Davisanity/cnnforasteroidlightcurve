import os
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
import random
import pandas as pd
import time
import sys
import keras
from keras.models import Sequential,load_model,Model
from keras.layers import Dense, Activation,Dropout, Flatten ,Conv2D, MaxPooling2D
from keras import losses
from keras.optimizers import Adam
from keras import backend as K
from keras.callbacks import TensorBoard

def getData(arg):
    path_list=[]
    for root,dirs,files in os.walk(arg+'\\'):
        # print(root) #data\ or data\other
        for f in files:
            p = os.path.join(root,f)
            path_list.append(p)
            # print(os.path.join(root,f)) #data\other\32033.png
    return path_list

def get_class(data):
    y=np.zeros([len(data)])
    for i in range(len(data)):
        if data[i].find('shapeW')!=-1 or data[i].find('genW')!=-1:
            y[i]=0
        elif data[i].find('shapeV')!=-1 or data[i].find('genV')!=-1:
            y[i]=1
        else:
            y[i]=2
    return y

def img_to_array(img_path):
    l = len(img_path) #l is 5, that is batch_size
    img_list=np.zeros((l,300,300,1))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).reshape(300,300,1)
        img_list[i]=img
        img_list[i]=img_list[i]/255
    # print("img_list.shape: ",img_list.shape) # output:(len(training_data_path),300,300,1))
    #img_list is a tuple so not img_list.shape()[tuple can't call function, img_list.shape is an attribute]
    return img_list

def main():
    data = sys.argv[1]
    model_name = sys.argv[2]
    output_layer_name = sys.argv[3]
    # vgg16 是 'dense_4',其他都是 'dense_3'
    threshold = float(sys.argv[4])
    data_path = getData(data)
    data_size = len(data_path)
    y_class=get_class(data_path) #for verification
    y_predict=np.zeros([len(data_path)])
    model = load_model(model_name)

    layer_output_model = Model(input=model.input,output=model.get_layer(output_layer_name).output)
    # print(y_class) #for verification
    # print("P R Name       val") # P for predict, R for real, Name for image name, val for softmax value
    tStart = time.time()
    for i in range(data_size):
        arr=[]
        arr.append(data_path[i])
        img_arr = img_to_array(arr)
        name = data_path[i].split("\\")
        value=layer_output_model.predict(img_arr)
        # #for verification ######
        # valuestr=str(value[0][0])+"_"+str(value[0][1])+"_"+str(value[0][2])
        # if y_class[i] == 0:
        #     c = 'w'
        # elif y_class[i] == 1:
        #     c = 'v'
        # elif y_class[i] ==2:
        #     c = 'o'
        # #for verification ######
        img = Image.open(data_path[i])
        if np.max(value)>threshold:
            output=value.argmax(axis=-1)
            y_predict[i]=output
            if output==0:
                # print("W "+c+" "+name[2]+" "+str(value))
                img.save("predict/W/"+name[2],"PNG")

            elif output==1:
                # print("V "+c+" "+name[2]+" "+str(value))
                img.save("predict/V/"+name[2],"PNG")

            elif output==2:
                # print("O "+c+" "+name[2]+" "+str(value))
                img.save("predict/O/"+name[2],"PNG")
        else:
            y_predict[i]=3
            # print("A "+"a"+" "+name[2]+" "+str(value))
            img.save("predict/A/"+name[2],"PNG")
        if y_class[i] != y_predict[i]:
             print(str(y_class[i])+" "+str(y_predict[i])+" "+name[1]+"/"+name[2]+" "+str(value))
    tEnd = time.time()
    print ("it costs %f sec" % (tEnd-tStart))
    # #for verification
    print("\t[Info] Display Confusion Matrix:") 
    print("\t0 for W, 1 for V, 2 for O, 3 for Ambigious")  
    print("%s\n" % pd.crosstab(y_class,y_predict,rownames=['label'],colnames=['predict']))  
    # #for verification
 
if __name__ == '__main__':
    try:
        main()
    except IndexError:
        print("Error: you need 4 args to execute this program!!")
        print("Need 4 args: predict_data       model_name       output_layer_name  threshold")
        print("Data types :    string            string              string          float" )
        print("For example:    'data'    'keras_model/6cnn.h5'      'dense_3'         0.91 ")
