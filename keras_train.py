
import os
import numpy as np
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
from PIL import Image
import PIL.ImageOps 
import random
import sys
import keras
from keras.models import Sequential,Model
from keras.layers import Dense, Activation, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.layers import BatchNormalization,concatenate,AveragePooling2D,concatenate,Input
from keras import losses
from keras.optimizers import SGD,Adam
from keras import backend as K
from keras.callbacks import TensorBoard
from keras.applications.resnet50 import ResNet50
import tensorflow as tf

num_class = 3
input_shape=(300,300,1)

def getData(arg):
    path_list=[]
    for root,dirs,files in os.walk(arg+'//'):
        # print(root) #data\ or data\other
        for f in files:
            p = os.path.join(root,f)
            path_list.append(p)
            # print(os.path.join(root,f)) #data\other\32033.png
    return path_list

def getOneHot(data):
    y=np.zeros([len(data),num_class])
    for i in range(len(data)):
        if data[i].find('shapeW')!=-1 or data[i].find('genW')!=-1:
            y[i][0]=1
        elif data[i].find('shapeV')!=-1 or data[i].find('genV')!=-1:
            y[i][1]=1
        else:
            y[i][2]=1
    return y

def img_to_array(img_path):
    l = len(img_path) #l is 5, that is batch_size
    img_list=np.zeros((l,300,300,1))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).reshape(300,300,1)
        img_list[i]=img
        img_list[i]=img_list[i]/255
    # print("img_list.shape: ",img_list.shape) # output:(len(training_data_path),300,300,1))
    #img_list is a tuple so not img_list.shape()[tuple can't call function, img_list.shape is an attribute]
    return img_list

def flatten_array(img_path):
    l = len(img_path)
    img_list=np.zeros((l,300*300))
    for i in range(len(img_path)):
        img = Image.open(img_path[i])
        img = img.resize((300,300),Image.BILINEAR)
        img = img.convert("L")
        img = PIL.ImageOps.invert(img) #White to Black (it means 255 to 0)
        img = np.array(img).flatten('F')
        img_list[i]=img/255
    return img_list

def generate_batch_data_random(inputs,batch_size):
    while True:
        random.shuffle(inputs)
        batch_inputs = inputs[0:batch_size]
        inputs = inputs[batch_size:] + inputs[:batch_size]
        img,label=img_to_array(batch_inputs),getOneHot(batch_inputs)
        # img,label=flatten_array(batch_inputs),getOneHot(batch_inputs) #flatten for FCN
        yield img,label
def Cnn2():
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3,3),activation='relu',padding='same',input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(3,3),strides=(2,2)))
    model.add(Conv2D(64, kernel_size=(3,3),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(3,3),strides=(2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_class, activation='softmax'))
    return model

def Cnn4():
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same',input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(256, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.7))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.6))
    model.add(Dense(num_class, activation='softmax'))
    return model
#no aug dropout 0.7 0.6
def Cnn6():
    model = Sequential()
    model.add(Conv2D(32,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same',input_shape=input_shape))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(32,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(64,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(64,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    
    model.add(Conv2D(128,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Conv2D(128,kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_class, activation='softmax'))
    return model
# no aug dropout0.7

def Cnn8():
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same',input_shape=input_shape))
    model.add(Conv2D(32, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    
    model.add(Conv2D(64, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(Conv2D(64, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    
    model.add(Conv2D(128, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(Conv2D(128, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    
    model.add(Conv2D(256, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(Conv2D(256, kernel_size=(3,3),strides=(1,1),activation='relu',padding='same'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    
    model.add(Flatten())
    model.add(Dense(128, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_class, activation='softmax'))
    return model

def VGG16Net(): 
    model = Sequential() 
    model.add(Conv2D(32,(3,3),strides=(1,1),input_shape=input_shape,padding='same',activation='relu'))
    model.add(Conv2D(32,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(64,(3,2),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(64,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(128,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(128,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(256,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(256,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(256,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(512,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(512,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(Conv2D(512,(3,3),strides=(1,1),padding='same',activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(1024,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(512,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(256,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_class,activation='softmax'))
    return model

def FCN():
    model = Sequential() 
    model.add(Dense(1024,input_dim=300*300,activation='relu'))
    model.add(Dropout(0.7))
    model.add(Dense(512,activation='relu'))
    model.add(Dropout(0.7))
    model.add(Dense(256,activation='relu'))
    model.add(Dropout(0.7))
    model.add(Dense(num_class,activation='softmax'))
    return model
# FCN 1024 512 256 dropout0.7 0.7 0.7
# FCN 1024 512 256 dropout0.8

# ResourceExhaustedError: OOM when allocating tensor with shape[100,256,75,75] and type float on /job:localhost/replica:0/task:0/device:GPU:0 by allocator GPU_0_bfc
# Out Of Memory QQ
def ResNet():
    resnet_conv = ResNet50(include_top=False, weights=None,input_tensor=None, input_shape=(300,300,1),pooling=None)
    out = resnet_conv.output
    out = Flatten()(out)
    out = Dropout(0.5)(out)
    output_layer = Dense(num_class,activation='softmax')(out)
    model = Model(inputs=resnet_conv.input,outputs=output_layer)
    return model
############### Google Net ####################
def Conv2d_BN(x, nb_filter,kernel_size, padding='same',strides=(1,1),name=None):
    if name is not None:
        bn_name = name + '_bn'
        conv_name = name + '_conv'
    else:
        bn_name = None
        conv_name = None

    x = Conv2D(nb_filter,kernel_size,padding=padding,strides=strides,activation='relu',name=conv_name)(x)
    x = BatchNormalization(axis=3,name=bn_name)(x)
    return x
  
#Define Inception structure
def Inception(x,nb_filter_para):
    (branch1,branch2,branch3,branch4)= nb_filter_para
    branch1x1 = Conv2D(branch1[0],(1,1), padding='same',strides=(1,1),name=None)(x)

    branch3x3 = Conv2D(branch2[0],(1,1), padding='same',strides=(1,1),name=None)(x)
    branch3x3 = Conv2D(branch2[1],(3,3), padding='same',strides=(1,1),name=None)(branch3x3)

    branch5x5 = Conv2D(branch3[0],(1,1), padding='same',strides=(1,1),name=None)(x)
    branch5x5 = Conv2D(branch3[1],(1,1), padding='same',strides=(1,1),name=None)(branch5x5)

    branchpool = MaxPooling2D(pool_size=(3,3),strides=(1,1),padding='same')(x)
    branchpool = Conv2D(branch4[0],(1,1),padding='same',strides=(1,1),name=None)(branchpool)

    x = concatenate([branch1x1,branch3x3,branch5x5,branchpool],axis=3)

    return x
  
#Build InceptionV1 model
def InceptionV1():
    classes=num_class
    inpt = Input(shape=input_shape)

    x = Conv2d_BN(inpt,64,(7,7),strides=(2,2),padding='same')
    x = MaxPooling2D(pool_size=(3,3),strides=(2,2),padding='same')(x)
    x = Conv2d_BN(x,192,(3,3),strides=(1,1),padding='same')
    x = MaxPooling2D(pool_size=(3,3),strides=(2,2),padding='same')(x)

    x = Inception(x,[(64,),(96,128),(16,32),(32,)]) #Inception 3a 28x28x256
    x = Inception(x,[(128,),(128,192),(32,96),(64,)]) #Inception 3b 28x28x480
    x = MaxPooling2D(pool_size=(3,3),strides=(2,2),padding='same')(x) #14x14x480

    x = Inception(x,[(192,),(96,208),(16,48),(64,)]) #Inception 4a 14x14x512
    x = Inception(x,[(160,),(112,224),(24,64),(64,)]) #Inception 4a 14x14x512
    x = Inception(x,[(128,),(128,256),(24,64),(64,)]) #Inception 4a 14x14x512
    x = Inception(x,[(112,),(144,288),(32,64),(64,)]) #Inception 4a 14x14x528
    x = Inception(x,[(256,),(160,320),(32,128),(128,)]) #Inception 4a 14x14x832
    x = MaxPooling2D(pool_size=(3,3),strides=(2,2),padding='same')(x) #7x7x832

    x = Inception(x,[(256,),(160,320),(32,128),(128,)]) #Inception 5a 7x7x832
    x = Inception(x,[(384,),(192,384),(48,128),(128,)]) #Inception 5b 7x7x1024

    #Using AveragePooling replace flatten
    x = AveragePooling2D(pool_size=(7,7),strides=(7,7),padding='same')(x)
    x =Flatten()(x)
    x = Dropout(0.4)(x)
    x = Dense(1000,activation='relu')(x)
    x = Dense(classes,activation='softmax')(x)
    
    model=Model(input=inpt,output=x)
    
    return model


def model_for_training(model):
    model_dict = {
        '2cnn':Cnn2,
        '4cnn':Cnn4,
        '6cnn': Cnn6,
        '8cnn':Cnn8,
        'vgg16':VGG16Net,
        'ResNet': ResNet,
        'FCN': FCN,
        'Inception':InceptionV1,
        
    }
    method = model_dict.get(model)
    return method

def Train(batch_size,epochs,lrn_rate,log_dir,model_name,data_path,valid_data_path,model):
    model = model
    adam = Adam(lr=lrn_rate)
    sgd = SGD(lr=lrn_rate)
    sgd_momentum = SGD(lr=lrn_rate, momentum=0.9)
    #######　Optimizer #######
    # model.compile(loss=keras.losses.categorical_crossentropy,optimizer=sgd_momentum,metrics=['accuracy'])
    model.compile(loss=keras.losses.categorical_crossentropy,optimizer=adam,metrics=['accuracy'])
    
    model.summary()
    tbCallBack = TensorBoard(log_dir=log_dir,  # log 目录
        histogram_freq=0,  # 按照何等频率（epoch）来計算直方圖，0為不計算
        write_graph=True,  # 是否儲存網路圖
        write_grads=True, # 是否視覺化梯度直方圖
        write_images=True,# 是否視覺化參數
        embeddings_freq=0, 
        embeddings_layer_names=None, 
        embeddings_metadata=None,
        update_freq='batch')
    train_history = model.fit_generator(generate_batch_data_random(data_path,batch_size),steps_per_epoch=len(data_path)/batch_size
        ,epochs=epochs,verbose=1,shuffle=True
        ,validation_data=generate_batch_data_random(valid_data_path,batch_size)
        ,validation_steps=len(valid_data_path)/batch_size,callbacks=[tbCallBack])   
    #steps_per_epoch：split a epoch into batch_size。ex:N=1000,steps_per_epoch = 10,that means batch_size=100
    #validation_steps:split a epoch into batch_size。ex:N=1000,steps_per_epoch = 10,that means batch_size=10
    score = model.evaluate(img_to_array(valid_data_path), getOneHot(valid_data_path), batch_size=batch_size, verbose=1)
    
    #flatten for FCN
    # score = model.evaluate(flatten_array(valid_data_path),getOneHot(valid_data_path),batch_size=batch_size, verbose=1)  
    
    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    model.save(model_name) #save after Training

def main():
    batch_size=int(sys.argv[1])
    epochs = int(sys.argv[2])
    lrn_rate=float(sys.argv[3])
    using_model=sys.argv[4]
    log_dir = sys.argv[5]
    model_name = sys.argv[6]
    train='train'
    test='valid'
    data_path = getData(train)
    valid_data_path = getData(test)
    model = model_for_training(using_model)()
    Train(batch_size,epochs,lrn_rate,log_dir,model_name,data_path,valid_data_path,model)
    
    

if __name__ == '__main__':
    try:
        main()
    except IndexError:
        print("Error: you need 6 args to execute this program!!")
        print("Need 6 args: batch_size  epochs  learing_rate  using_model    log_dir             model_name")
        print("Data types :    int       int        float        string      string                string")
        print("For example:    100        2         1e-3        '6cnn'   './keras_log/ARGS'   './keras_model/ARGS.h5' ")